import { VideoContentRating } from "./VideoContentRating";

export interface VideoContentDetails {
  duration: string;
  dimension: string;
  definition: string;
  caption: string;
  licensedContent: boolean;
  contentRating: VideoContentRating;
  projection: string;
}
