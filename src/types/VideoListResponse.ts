import { VideoItem } from "./VideoItem";
import { PageInfo } from "./PageInfo";

export interface VideoListResponse {
  kind: string;
  etag: string;
  items: VideoItem[];
  nextPageToken?: string;
  prevPageToken?: string;
  pageInfo: PageInfo;
}
