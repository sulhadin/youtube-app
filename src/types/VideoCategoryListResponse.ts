import { VideoCategoryItem } from "./VideoCategoryItem";

export interface VideoCategoryListResponse {
  kind: string;
  etag: string;
  items: VideoCategoryItem[];
}
