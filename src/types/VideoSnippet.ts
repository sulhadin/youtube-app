import { Thumbnails } from "./Thumbnails";
import { Localized } from "./Localized";

export interface VideoSnippet {
  publishedAt: Date;
  channelId: string;
  title: string;
  description: string;
  thumbnails: Thumbnails;
  channelTitle: string;
  tags?: string[];
  categoryId: string;
  liveBroadcastContent: string;
  localized: Localized;
  defaultLanguage?: string;
  defaultAudioLanguage?: string;
}
