import { VideoSnippet } from "./VideoSnippet";
import { VideoContentDetails } from "./VideoContentDetails";
import { VideoStatistics } from "./VideoStatistics";

export interface VideoItem {
  kind: string;
  etag: string;
  id: string;
  snippet: VideoSnippet;
  contentDetails: VideoContentDetails;
  statistics: VideoStatistics;
}
