import React from "react";
import { Link, useHistory } from "react-router-dom";
import { VideoCard } from "../../components";
import { useVideoListContainer } from "./useVideoListContainer";
import { scrollUp } from "../../libs/scrollUp";

export const VideoListContainer = () => {
  const {
    videos,
    highlightedCategories,
    videoCategoryId,
    hasNextPage,
    hasPrevPage,
    nextURL,
    prevURL,
  } = useVideoListContainer();

  const history = useHistory();

  return (
    <div className="container" data-testid="container-video-list">
      <div className="row mt-5">
        <div className="col-md-12 text-center">
          <button
            key={`VideoCategory_All`}
            type="button"
            className={`btn m-2 ${
              !videoCategoryId ? "btn-primary" : "btn-link"
            }`}
            onClick={() => history.push(`/`)}
          >
            All Categories
          </button>

          {highlightedCategories.map((item) => (
            <button
              key={`VideoCategory_${item.id}`}
              data-testid={`action-load-category-${item.id}`}
              type="button"
              className={`btn m-2 ${
                videoCategoryId === item.id ? "btn-primary" : "btn-link"
              }`}
              onClick={() => history.push(`/${item.id}`)}
            >
              {item.snippet.title}
            </button>
          ))}
        </div>
      </div>

      <div className="row mt-5">
        <div className="col-md-12">
          <div className="d-flex flex-column align-items-center">
            {videos.map((video) => (
              <VideoCard
                key={`VideoCard_${video.id}`}
                id={video.id}
                thumbnail={
                  video.snippet.thumbnails.standard ??
                  video.snippet.thumbnails.default
                }
                title={video.snippet.title}
                date={video.snippet.publishedAt}
              />
            ))}
          </div>
        </div>
      </div>
      <div className="row mt-5 mb-5">
        <nav aria-label="Page navigation example">
          <ul className="pagination justify-content-center">
            <li className={`page-item ${!hasPrevPage && "disabled"}`}>
              <Link
                to={prevURL}
                onClick={scrollUp}
                className="page-link"
                data-testid="action-prev-page"
              >
                Previous
              </Link>
            </li>
            <li className={`page-item ${!hasNextPage && "disabled"}`}>
              <Link
                to={nextURL}
                onClick={scrollUp}
                className="page-link"
                data-testid="action-next-page"
              >
                Next
              </Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
};
