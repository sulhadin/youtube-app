import React from "react";
import { Switch, Route } from "react-router-dom";
import { VideoListContainer } from "./VideoListContainer";
import { VideoDetailContainer } from "./VideoDetailContainer";
import { Loading } from "../components";
import "../assets/styles/index.scss";

export const App = () => {
  return (
    <>
      <Loading />
      <Switch>
        <Route exact path="/">
          <VideoListContainer />
        </Route>
        <Route exact path="/:categoryId">
          <VideoListContainer />
        </Route>
        <Route exact path="/video/:videoId">
          <VideoDetailContainer />
        </Route>
      </Switch>
    </>
  );
};
