import React from "react";
import ReactDOM from "react-dom";
import { AppRoute } from "./containers/AppRoute";

ReactDOM.render(<AppRoute />, document.getElementById("root"));
