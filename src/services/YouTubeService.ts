import { NetworkClient } from "../libs/NetworkClient";
import { BaseService } from "./BaseService";
import { VideoListResponse } from "../types/VideoListResponse";
import { VideoCategoryListResponse } from "../types/VideoCategoryListResponse";

export class YouTubeService extends BaseService {
  getVideos(params: {
    id?: string;
    part: string;
    chart?: "mostPopular";
    maxResults?: number;
    pageToken?: string;
    videoCategoryId?: string;
    regionCode?: string;
  }) {
    return this.networkClient
      .getNetworkInstance()
      .get<VideoListResponse>("/videos", { params });
  }

  getVideoCategories(params: { part: string; regionCode: string }) {
    return this.networkClient
      .getNetworkInstance()
      .get<VideoCategoryListResponse>("/videoCategories", { params });
  }
}

const youtubeClient = new NetworkClient(
  "https://www.googleapis.com/youtube/v3"
);

youtubeClient.getNetworkInstance().interceptors.request.use((config) => {
  config.params = {
    ...config.params,
    key: process.env.YOUTUBE_API_KEY,
  };
  return config;
});

export const youTubeService = new YouTubeService(youtubeClient);
