import { INetworkClient } from "../interfaces/INetworkClient";

export class BaseService {
  protected networkClient: INetworkClient;

  constructor(networkClient: INetworkClient) {
    this.networkClient = networkClient;
  }
}
