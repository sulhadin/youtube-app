import { loadingEvent } from "./loadingEvent";

export const useLoading = () => {
  const showLoading = () =>
    loadingEvent.dispatchEvent(new CustomEvent("onShowLoading"));
  const hideLoading = () =>
    loadingEvent.dispatchEvent(new CustomEvent("onHideLoading"));

  return { showLoading, hideLoading };
};
