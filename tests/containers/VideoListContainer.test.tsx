import { YouTubeService } from "../../src/services";
import { renderWithRouter } from "../testUtils";
import { App } from "../../src/containers/App";
import { screen } from "@testing-library/react";
import React from "react";
import "@testing-library/jest-dom";
import userEvent from "@testing-library/user-event";

window.scrollTo = jest.fn();
const getVideosSpy = jest.spyOn(YouTubeService.prototype, "getVideos");

afterEach(() => {
  jest.clearAllMocks();
});

describe("test VideoListContainer - all categories (end-to-end)", function () {
  test("when user open the page, getVideos should be called", async () => {
    renderWithRouter(<App />);

    // Wait for initial state
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    expect(getVideosSpy).toBeCalledTimes(1);
    expect(getVideosSpy).toBeCalledWith({
      chart: "mostPopular",
      maxResults: 10,
      part: "snippet,contentDetails,statistics",
      regionCode: "GB",
      pageToken: null,
      videoCategoryId: null,
    });
  });

  test("the next page button should call next page", async () => {
    renderWithRouter(<App />);

    // Wait for initial state
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    // Click the next page button
    userEvent.click(await screen.findByTestId("action-next-page"));
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    expect(window.scrollTo).toBeCalledTimes(1);
    expect(getVideosSpy).toBeCalledTimes(2);
    expect(getVideosSpy).toHaveBeenLastCalledWith({
      chart: "mostPopular",
      maxResults: 10,
      part: "snippet,contentDetails,statistics",
      regionCode: "GB",
      pageToken: expect.any(String),
      videoCategoryId: null,
    });
  });

  test("the previous page button should call previous page", async () => {
    renderWithRouter(<App />);

    // Wait for initial state
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    // Click the next page button
    userEvent.click(await screen.findByTestId("action-next-page"));
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    // Click the prev page button
    userEvent.click(await screen.findByTestId("action-prev-page"));
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    expect(getVideosSpy).toBeCalledTimes(3);
    expect(getVideosSpy).toHaveBeenLastCalledWith({
      chart: "mostPopular",
      maxResults: 10,
      part: "snippet,contentDetails,statistics",
      regionCode: "GB",
      pageToken: expect.any(String),
      videoCategoryId: null,
    });
  });
});

describe("test VideoListContainer - sub category (end-to-end)", function () {
  test("when user choose a category, getVideos should be called", async () => {
    renderWithRouter(<App />);

    // Wait for initial state
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    // Choose category
    userEvent.click(await screen.findByTestId("action-load-category-10"));
    expect(
      await screen.findByTestId("component-no-loading")
    ).toBeInTheDocument();

    expect(getVideosSpy).toBeCalledTimes(2);
    expect(getVideosSpy).toHaveBeenLastCalledWith({
      chart: "mostPopular",
      maxResults: 10,
      part: "snippet,contentDetails,statistics",
      regionCode: "GB",
      pageToken: null,
      videoCategoryId: "10",
    });
  });
});
