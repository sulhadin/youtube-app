FROM node:14-alpine AS BUILD_STAGE
WORKDIR /workspace
COPY package.json yarn.lock ./
RUN yarn
COPY ./ ./
RUN yarn test
RUN yarn buildWithWebpack
RUN cp dist/index.html dist/200.html

FROM scratch AS EXPORT_STAGE
COPY --from=BUILD_STAGE /workspace/dist ./
